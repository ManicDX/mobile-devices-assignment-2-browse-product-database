package com.example.m100266177.assignment2_marcevans100266177;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import java.util.Random;

/**
 * Created by 100266177 on 27/10/2015.
 */
public class AddProductActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product_activity);

        Intent intent = getIntent();
    }

    public void saveButtonHandler(View view){

        EditText addName = (EditText)findViewById(R.id.addName_editText);
        EditText addDescription = (EditText)findViewById(R.id.addDescription_editText);
        EditText addPrice = (EditText)findViewById(R.id.addPrice_editText);

        String name = addName.getText().toString();
        String description = addDescription.getText().toString();
        String price = addPrice.getText().toString();


        Product product = new Product(name, description, Double.parseDouble(price));

        //TODO temp rand int key
        Random r = new Random();
        product.productId = r.nextLong();

        BrowseProductsActivity.dbHelper.insertProduct(product);

        clearText();
        finish();
    }

    public void cancelButtonHandler(View view){
        clearText();
        finish();
    }

    private void clearText(){

        EditText addName = (EditText)findViewById(R.id.addName_editText);
        EditText addDescription = (EditText)findViewById(R.id.addDescription_editText);
        EditText addPrice = (EditText)findViewById(R.id.addPrice_editText);

        addName.setText("");
        addDescription.setText("");
        addPrice.setText("");
    }
}
