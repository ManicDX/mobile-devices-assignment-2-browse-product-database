package com.example.m100266177.assignment2_marcevans100266177;

/**
 * Created by 100266177 on 27/10/2015.
 */
public class Product {
    public long      productId;
    public String   name;
    public String   description;
    public double    price;

    Product(String _name, String _description, double _price){
        name = _name;
        description = _description;
        price = _price;
    }

    //TODO getters/setters
}
