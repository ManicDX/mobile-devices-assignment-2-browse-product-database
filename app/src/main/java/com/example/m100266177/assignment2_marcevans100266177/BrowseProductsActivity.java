package com.example.m100266177.assignment2_marcevans100266177;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class BrowseProductsActivity extends Activity {


    ////public static ProductDBHelper database = new ProductDBHelper();
    public static ProductDBHelper dbHelper;
    public ArrayList<Product> productList = new ArrayList<>();
    public static final int code = 1010001;
    
    private int currentProduct = 0;
    private Resources res;
    private String f_url = "https://blockchain.info/tobtc?currency=CAD&value=49.99";
    private static Product empty;


    protected String data = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browse_products_activity);


        dbHelper = new ProductDBHelper(this);



        //Test data
        Product p1 = new Product("Baton", "It's a Baton", 3.99);
        Product p2 = new Product("Book", "It's a Book", 33.99);
        Product p3 = new Product("BatMan", "I'm Batman", 666.69);
        dbHelper.insertProduct(p1);
        dbHelper.insertProduct(p2);
        dbHelper.insertProduct(p3);
        productList = dbHelper.queryDatabase();

        //default empty product
        empty = new Product("", "", 0.0);


        res = getResources();

        //Show first Product
        showProduct(productList.get(currentProduct));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_browse_products, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Show the values of the current product in the activity's text labels
    public void showProduct(Product product){
        TextView productName = (TextView)findViewById(R.id.productName_textView);
        TextView productDescription = (TextView)findViewById(R.id.productDescription_textView);
        TextView priceView = (TextView)findViewById(R.id.price_textView);
        TextView bitCoinView = (TextView)findViewById(R.id.bitcoinPrice_textView);

        productName.setText(res.getString(R.string.name_text) + product.name);
        productDescription.setText(product.description);
        priceView.setText(res.getString(R.string.price_text) + Double.toString(product.price));

        //bitCoin conversion of price
        Double bitCoin = 0.0;
        try {
            bitCoin = convertToBitCoin(product.price);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        bitCoinView.setText(res.getString(R.string.bitcoinPrice_text) + String.format("%.2f", bitCoin));

    }

    //bitCoin conversion of price
    public double convertToBitCoin(double price) throws InterruptedException {

        //Get bit-coin conversion rate from url
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(f_url);
                    HttpURLConnection conn = null;
                    conn = (HttpURLConnection) url.openConnection();
                    InputStream in = conn.getInputStream();
                    //data = in.toString();
                    data = getStringFromInputStream(in);

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("Error", "No Connection");
                }
            }
        }
        );

        t.start(); // spawn thread

        t.join();  // wait for thread to finish

        Log.e("ERROR", "" + Double.parseDouble(data));
        return price * Double.parseDouble(data);
    }

    //Subtract from currentProduct, if at zero already skip
    public void prevButtonHandler(View view){
        if(currentProduct <= 0)
            return;

        currentProduct--;
        showProduct(productList.get(currentProduct));
    }

    //Add to currentProduct, if at max size already skip
    public void nextButtonHandler(View view){
        int size = productList.size();
        if(currentProduct >= size - 1)
            return;

        currentProduct++;
        showProduct(productList.get(currentProduct));
    }

    //Delete current product, re-query database
    public void deleteButtonHandler(View view) {
        if(productList.size() <= 1)
            return;

        long id = productList.get(currentProduct).productId;
        dbHelper.deleteProduct(id);
        productList = dbHelper.queryDatabase();

        if(currentProduct >= productList.size())
            currentProduct--;

        if(currentProduct >= 0) {
            showProduct(productList.get(currentProduct));
        } else {
            showProduct(empty);
        }
    }

    public void addProductHandler(View view){
        Intent intent = new Intent(this, AddProductActivity.class);
        startActivityForResult(intent, code);
    }


    @Override
    public void onActivityResult(int requestCode, int responseCode, Intent resultIntent) {
        super.onActivityResult(requestCode, responseCode, resultIntent);

        //refresh database if product added
        if(requestCode == code){
            productList = dbHelper.queryDatabase();
            if(productList.size() > 0)
                showProduct(productList.get(currentProduct));
        }

    }



    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();

    }

}
