package com.example.m100266177.assignment2_marcevans100266177;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by 100266177 on 27/10/2015.
 */

//TODO change to extends SQLiteOpenHelper
//public class ProductDBHelper{
//    //TODO change to table database
//    ArrayList<Product> productTable = new ArrayList<>();
//
//    ProductDBHelper(){
//        //TODO add database retrieval, remove temp products
//        Product p1 = new Product(1001, "Baton", "It's a Baton", 3.99);
//        Product p2 = new Product(1011, "Book", "It's a Book", 33.99);
//        Product p3 = new Product(1111, "BatMan", "I'm Batman", 666.69);
//        productTable.add(p1);
//        productTable.add(p2);
//        productTable.add(p3);
//    }
//
//    public ArrayList<Product> queryDatabase(){
//        return productTable;
//    }
//
//    public void insertProduct(Product p){
//        productTable.add(p);
//    }
//
//    public void deleteProduct(int iD){
//        productTable.remove(iD);
//    }
//}

public class ProductDBHelper extends SQLiteOpenHelper{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_FILENAME = "products.db";
    public static final String TABLE_NAME = "Products";

    public static final String  productID =             "_id";
    public static final String  productName =           "productName";
    public static final String  productDescription =    "productDescription";
    public static final String  productPrice =          "price";

    // don't forget to use the column name '_id' for your primary key
    public static final String CREATE_STATEMENT = "CREATE TABLE " + TABLE_NAME + "(" +
            " " + productID + " integer primary key autoincrement," +
            " " + productName + " text not null," +
            " " + productDescription + " text not null," +
            " " + productPrice + " text not null" +
            ")";

    public static final String DROP_STATEMENT = "DROP TABLE " + TABLE_NAME;

    public ProductDBHelper(Context context) {
        super(context, DATABASE_FILENAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(CREATE_STATEMENT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        // the implementation below is adequate for the first version
        // however, if we change our table at all, we'd need to execute code to move the data
        // to the new table structure, then delete the old tables (renaming the new ones)

        // the current version destroys all existing data
        database.execSQL(DROP_STATEMENT);
        database.execSQL(CREATE_STATEMENT);
    }


    public ArrayList<Product> queryDatabase(){
        ArrayList<Product> productList = new ArrayList<>();

        // obtain a database connection
        SQLiteDatabase database = this.getWritableDatabase();

        // retrieve the contact from the database
        String[] columns = new String[] {productID, productName, productDescription, productPrice};
        Cursor cursor = database.query(TABLE_NAME, columns, "", new String[]{}, "", "", "");

        cursor.moveToFirst();
        do {
            // collect the contact data, and place it into a contact object
            long id = Long.parseLong(cursor.getString(0));
            String name = cursor.getString(1);
            String desc = cursor.getString(2);
            String price = cursor.getString(3);
            Product product = new Product(name, desc, Double.parseDouble(price));
            product.productId = id;

            // add the current contact to the list
            productList.add(product);

            // advance to the next row in the results
            cursor.moveToNext();
        } while (!cursor.isAfterLast());



        Log.i("DatabaseAccess", "queryDatabase():  num: " + productList.size());

        return productList;
    }

    public void insertProduct(Product p){

        // obtain a database connection
        SQLiteDatabase database = this.getWritableDatabase();

        // insert the data into the database
        ContentValues values = new ContentValues();
        values.put(productName, p.name);
        values.put(productDescription, p.description);
        values.put(productPrice, p.price);
        p.productId = database.insert(TABLE_NAME, null, values);
    }

    public void deleteProduct(long iD){
        // obtain a database connection
        SQLiteDatabase database = this.getWritableDatabase();

        // delete the contact
        int numRowsAffected = database.delete(TABLE_NAME, "_id = ?", new String[] { "" + iD });

        Log.i("DatabaseAccess", "deleteProduct(" + iD + "):  numRowsAffected: " + numRowsAffected);

        // verify that the contact was deleted successfully
        //return (numRowsAffected == 1);  //TODO change to boolean function?
    }
}
